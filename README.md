# dtach

## Module description

The `dtach` module manages long-lived dtach sessions as securely as
possible. The primary use case is to run `irssi` bouncers more
securely than just shelling into a server and running it under screen
as your primary user.

This module was implemented in response to the 2021 screen remote code
execution vulnerabilities, but the actual systemd unit file and dtach
concept was actually [implemented in March 2019](https://anarc.at/blog/2019-03-05-report/#using-dtach-instead-of-screen-for-my-irc-bouncer) after realizing I
had a long-running, highly privileged network process running as my
primary user on my shell server, which seemed... not ideal.

## Setup

The `dtach` module obviously requires a working [dtach](http://dtach.sourceforge.net/) package. To
run long-running sessions, you will also need the [systemd module](https://forge.puppet.com/modules/camptocamp/systemd).

## Usage

### irssi sessions

This will enable a long-running IRC session under the user `foo-irc`:

    class { 'dtach::irssi':
      user => foo-irc',
    }

You will need to add an SSH key to the user for this to be useful,
obviously. I recommend either this:

    restrict,pty,command="dtach -a /run/foo-irc/dtach-irssi.socket" ssh-ed25519 AAAA[...]

... or, if you want to use [mosh](https://mosh.org/):

    restrict,pty,command="/usr/local/bin/mosh-ssh-wrapper dtach -a /run/anarcat-irc/dtach-irssi.socket" ssh-ed25519 AAAA[...]

Make sure the `authorized_keys` file is not writable by the sandbox
user, as a security precaution (as that would it to run arbitrary code
by rewriting the file).

In my configuration, I make that key a password-less key, on disk, so
that I don't need to enter a password to login to IRC.

## Limitations

The systemd containment will break certain irssi functionality, on
purpose, most notably `/exec`.

The systemd containment could be improved. For example, it might be
better if it would run inside a real container?

We do not manage SSH keys (yet), as stated above. Obviously, this
module would benefit from deploying the SSH keys hack (or at least the
magic script) automatically.

We only support `irssi`, and it's possible that this is a stupid idea
in the first place, with better options, including:

 * irssi proxy
 * weechat relays
 * Matrix

But I've been running irssi for so long that this seemed simpler.

The module could use unit tests.
