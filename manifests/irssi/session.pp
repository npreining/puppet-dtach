# enable a single user session
define dtach::irssi::session {
  systemd::unit_file { "irssi@${name}.service":
    target => '/etc/systemd/system/irssi@.service',
    path   => '/etc/systemd/system/multi-user.target.wants/',
    enable => true,
    active => true,
  }
  user { $name:
    system     => true,
    managehome => true,
    shell      => '/bin/sh',
    comment    => "${name} IRC bouncer",
  }
}
