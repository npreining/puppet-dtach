# base class for detached programs
#
# This will be common to all programs managed by dtach
class dtach {
  package { 'dtach':
    ensure => present,
  }
  file { '/usr/local/bin/mosh-ssh-wrapper':
    source => 'puppet:///modules/dtach/mosh-ssh-wrapper',
    mode   => '0555',
  }
}
