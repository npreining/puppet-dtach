# run irssi detached under less privileges
#
# @param user if set, will also enable a session for that user
class dtach::irssi(
  Optional[String] $user = undef,
) {
  include dtach
  package { 'irssi':
    ensure => present,
  }
  systemd::unit_file { 'irssi@.service':
    source  => 'puppet:///modules/dtach/irssi@.service',
    enable  => false,
    active  => false,
    require => Package['irssi'],
  }
  # TODO: could be a list if we want to have multiple users in
  # hiera...
  if $user {
    dtach::irssi::session { $user : }
  }
}
